package com.glearning.orders.repository;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.glearning.orders.model.Order;

@Repository
public class OrderJpaRepository {
	
	private Set<Order> orders = new HashSet<>();
	
	public Order save(Order order) {
		this.orders.add(order);
		return order;
	}
	
	public Set<Order> fetchAll(){
		return this.orders;
	}
	
	public Order findById(int id) {
		return orders.stream().filter(order -> order.getId() == id).findAny().get();
	}
	
	public void deleteOrderById(int id) {
		orders.removeIf(order -> order.getId() == id);
	}

}
