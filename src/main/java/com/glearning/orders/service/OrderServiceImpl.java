package com.glearning.orders.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glearning.orders.model.Order;
import com.glearning.orders.repository.OrderJpaRepository;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderJpaRepository orderRepository;
	
	@Override
	public Order save(Order order) {
		return this.orderRepository.save(order);
	}

	@Override
	public Set<Order> fetchAll() {
		return this.orderRepository.fetchAll();
	}

	@Override
	public Order fetchOrderById(int id) {
		return this.orderRepository.findById(id);
	}

	@Override
	public void deleteOrderById(int id) {
	}
}
